describe('Sanity test', () => {
  it('Visit the app root url', () => {
    cy.visit('/');
    cy.contains('h1', 'Listen to Great Music!');
  });
});
