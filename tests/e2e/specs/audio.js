describe('Audio player', () => {
  it('play audio', () => {
    cy.visit('/');
    // wait songs to be loaded
    cy.wait(5000);
    cy.get('.composition-name:first').click();
    cy.get('#play-button').click();
    cy.wait(5000);
    cy.get('#player-play-button').click();
  });
});
