import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import songItem from '@/components/SongItem.vue';

describe('Snapshots SongItem', () => {
  test('render correctly', () => {
    const song = {
      docID: 'abc',
      modified_name: 'test',
      display_name: 'test',
      comment_count: 5,
    };
    const wrapper = shallowMount(songItem, {
      propsData: { song },
      global: {
        components: {
          'router-link': RouterLinkStub,
        },
      },
    });

    expect(wrapper.element).toMatchSnapshot();
  });
});
