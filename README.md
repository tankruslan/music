# Music App
App uses `VueJS` on frontend and `Firebase` as backend.

This app allows you listen to music uploaded by other users.
Also, it's possible to upload your own music after registration.
Open [demo](https://music-lake-omega.vercel.app/).

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```
