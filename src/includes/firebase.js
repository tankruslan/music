import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDpph64YDReezyygNrZ4v30KXTmvrKO-fk',
  authDomain: 'music-52312.firebaseapp.com',
  projectId: 'music-52312',
  storageBucket: 'music-52312.appspot.com',
  messagingSenderId: '297448708184',
  appId: '1:297448708184:web:11fa150aaf138c7ce8522d',
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();
const storage = firebase.storage();

db.enablePersistence().catch((error) => {
  console.log(`Firebase persistence error ${error.code}`);
});

const usersCollection = db.collection('users');
const songsCollection = db.collection('songs');
const commentsCollection = db.collection('comments');

export {
  auth,
  db,
  usersCollection,
  songsCollection,
  commentsCollection,
  storage,
};
